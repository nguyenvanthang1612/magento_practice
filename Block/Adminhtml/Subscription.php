<?php

namespace Packt\HelloWorld\Block\Adminhtml;

use Magento\Backend\Block\Widget\Grid\Container;

class Subscription extends Container
{
    protected function _construct()
    {
        $this->_blockGroup = 'Packt_HelloWorld';
        $this->_controller = 'adminhtml_subscription';
        $this->_headerText = __('Manage Posts');
        $this->_addButtonLabel = __('Add New Post');
        parent::_construct();
    }
}