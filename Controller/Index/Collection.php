<?php 

namespace Packt\HelloWorld\Controller\Index;

// use Zend_Debug;

class Collection extends \Magento\Framework\App\Action\Action
{
    public function execute() 
    {
        $productCollection = $this->_objectManager
            ->create('Magento\Catalog\Model\ResourceModel\Product\Collection')
            ->addAttributeToSelect([
                'name',
                'price',
                'image',
            ])
            // ->addAttributeToFilter('entity_id', array(
            //     'in' => array(159, 160, 161)
            // ));
            // ->addAttributeToFilter('name', 'Shoes');
            // ->setPageSize(10,1);
            ->addAttributeToFilter('name', array(
                'like' => '%Jac%'
            ));
        // die($productCollection->getSelect()->__toString());
        $output = '';

        $productCollection->setDataToAll('price', 20);

        $productCollection->save();

        foreach ($productCollection as $key => $product) 
        {
            $output .= var_dump($product->debug(), null, false);
        }

        $this->getResponse()->setBody($output);
    }
}